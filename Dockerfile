FROM node:10.11 AS build

WORKDIR /app
COPY [ "package.json", "./" ]
RUN npm install

FROM node:10.11 AS dev

WORKDIR /app
COPY --from=build [ "/app", "./"]
COPY [ "nodemon.json", ".babelrc", "local-dev.js", "./" ]
CMD [ "npm", "run", "dev:watch" ]
