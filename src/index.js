import createApp from "./app";

const app = createApp().listen(3000, () => {
  console.log(`Server is running on port ${app.address().port}`);
});
