import cors from "cors";
import bodyParser from "body-parser";

export default function configureMiddleware(app) {
  app.use(cors());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());
}
