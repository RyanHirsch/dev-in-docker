import express from "express";
import configureMiddleware from "./middleware";

export default function createApp() {
  const app = express();
  configureMiddleware(app);

  app.use("/", (_req, res) => res.json({ hey: "you" }));
  return app;
}
