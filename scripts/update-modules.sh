#!/usr/bin/env bash
set -e

APP_CONTAINER=$(docker-compose ps | grep app | awk '{print $1}')

docker container cp package.json $APP_CONTAINER:/app
docker container exec $APP_CONTAINER npm install

echo "Completed module installation, proactively rebuilding container..."
docker-compose build
